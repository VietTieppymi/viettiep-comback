Course outline:

   1 Intro, interpreter, number, boolean
   2 List, string, name
   3 GitLab, control flow, list
   4 Git, Tuple, PEP8, ListComps
   5 Import, Set, Dict
   6 File, JSON, Function, Exception
   7 Exception, Package, Virtualenv, Pip, Class
   8 stdlib, decorator, generator, debugging
   9 Crawling/Scraping data (a MUST for data analysis)
   10 Flask, API, DB
   11 Data analysis [numpy, matplotlib, pandas, a bit sklearn]
   12 Daily life Automation (not Ansible/SaltStack), QAs
