#!/usr/bin/env python3


def solve(numbers):
    '''Tìm phần tử lớn nhất của list số nguyên `numbers`
    Không sử dụng function `max`, `sorted`
    '''
    assert isinstance(numbers, list)
    max_val = 0
    input_dat = numbers
    for i in range(len(input_dat)):
        if input_dat[i] >= max_val:
            max_val = input_dat[i]
        else:
            max_val = max_val
    print(max_val)
    return max_val


def main():
    print(solve())


if __name__ == "__main__":
    main()
