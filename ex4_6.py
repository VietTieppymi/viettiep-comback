#!/usr/bin/env python3


def solve(text):
    '''Bóc tách từ `text` ra một list các số theo thứ tự chúng xuất hiện.

    VD: 'Em ơi có bao nhiêu, 60năm cuộc đời, 20 năm đầu, sung sướng0bao lâu'
    -> [60, 20, 0]

    NOTE: không dùng `re` library
    '''

    res = []
    result = []
    for word in text.split():
        res = []
        for w in range(len(word)):
            if word[w].isnumeric():
                restmp = w, str(word[w])
                res.append(restmp)
        if len(res) != 0:
            resword = ''
            # if len(res) > 1:
            # reslst = []
            for i in range(len(res)-1):
                if len(res) > 1:
                    # if i==0:
                    #     resword = res[0][1]
                    # # resword = res[0][1]
                    if int(res[i + 1][0]) - int(res[i][0]) == 1 and i != 0:
                        resword= resword+res[i][1]+res[i+1][1]
                        result.append(int(resword))
                    if int(res[i + 1][0]) - int(res[i][0]) != 1:
                        if i != 1:
                            result.append(int(res[i][1]))
                            result.append(int(res[i+1][1]))
                        else:
                            result.append(int(res[0][1]))
                            result.append(int(res[i][1]))
                       # resword = resword+','+res[i][1]
                else:
                    result.append(int(res[i][1]))
    return result


def main():
    # ss = 'Bé lên 3 bé đi lớp 4'
    # print(solve(ss))
    # assert solve(ss) == [3, 4]
    # ss = ', 60năm cuộc đời, 20 năm đầu, sung sướng0bao lâu'
    # print(solve(ss))
    # assert solve(ss) == [60, 20, 0]
    ss = '6năm0cuộcđời,20nămđầu,sung sướng0bao lâu'
    print(solve(ss))
    assert solve(ss) == [6, 0, 20, 0]


if __name__ == "__main__":
    main()
