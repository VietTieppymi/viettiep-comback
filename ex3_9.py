#!/usr/bin/env python3

'''
a, b, c là các số nguyên dương nhỏ hơn 10, biết a + b/c = 10

In ra list chứa các bộ số thỏa mãn điều kiện trên (a, b, c có thể trùng nhau).

Ví dụ:

- output: [[9, 1, 1], ...]
'''


def solve():
    '''Trả về list chứa các list là các bộ số thỏa mãn đề bài

    Ví dụ:
        [[9, 1, 1], ..., [1, 9, 1]]

    Lưu ý: kết quả từng list con trả về với a giảm dần, b và c tăng dần
    '''
    input1 = [9, 8, 7, 6, 5, 4, 3, 2, 1, 0]
    res1 = []
    res2 = []
    res3 = []
    res4 = []
    res5 = []
    res6 = []
    result=[]
    for i in range(10):
        res1.append(input1[i])
        res6.append(i)
        for j in range(10):
            res2 = [res1[i], j]
            res3.append(res2)
    for i, j in res3:
        for k in range(1, 10):
            if (10 - i) * k == j:
                res4 = [i, j, k]
                result.append(res4)
    return result

def main():
    print(solve())


if __name__ == "__main__":
    main()
