#!/usr/bin/env python3


def solve(input_data):
    '''Kiểm tra input_data có phải là palindrome không.

    Một string được gọi là `palindrome` nếu viết xuôi hay ngược đều thu được
    kết quả như nhau (không phân biệt hoa thường, bỏ qua dấu space).
    String phải dài hơn 2 chữ cái.
    Ví dụ các palindrome: 'civic', 'Able was I ere I saw Elba', 'Noon'

    :rtype: bool
    '''
    result = None
    input_data_stripped = input_data.strip()
    input_ltr = [input_data[i].lower() for i in range(len(input_data_stripped) // 2)]
    input_rtl = [input_data[-i - 1].lower() for i in range(len(input_data_stripped) // 2)]
    if (input_ltr == input_rtl) and (len(input_data_stripped) > 2):
        result = True
    else:
        result = False
    return result


def main():
    print(solve('Able was I ere I saw Elba'))


if __name__ == "__main__":
    main()
