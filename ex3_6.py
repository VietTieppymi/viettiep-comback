#!/usr/bin/env python3

'''
Input: một số nguyên trong range(1,13,1). # start=1, stop=13, step=1
Output: tên tương ứng của tháng đó bằng tiếng Anh, và số ngày trong tháng đó.
Tháng 2 tính 28 ngày.

Ví dụ:

- input_data: 2

- output: February 28
'''


def solve(input_data):
    '''Trả về 1 `list` chứa 2 phần tử, ví dụ:

        input_data: 2
        output: ("February", 28)

    (1,2) là biểu diễn tương tự [1,2], chỉ thay dấu ngoặc vuông thàntròn.
    Đây là kiểu dữ liệu tuple.
    :param input_data: tháng bất kì
    :rtype: list
    '''
    assert (input_data in range(1, 13, 1)), "Tháng không tồn tại"
    month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October',
             'November', 'December']
    tmp = [m.split() for m in month]
    for m in range(len(tmp)):
        if m in {0,2,4,6,7,9,11}:
            tmp[m].append(31)
        elif m in {1}:
            tmp[m].append(28)
        else:
            tmp[m].append(30)
    res = [tuple(t) for t in tmp]
    result = res[input_data-1]
    return result


def main():
    month, day = solve(2)
    print(month, day)


if __name__ == "__main__":
    main()
