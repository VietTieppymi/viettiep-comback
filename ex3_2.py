#!/usr/bin/env python3
"""
Read:

- https://pymi.vn/tutorial/string1/
- https://www.familug.org/2015/07/go-strings-package-xu-ly-string.html
"""

data = '''
Come to the
River
Of my
Soulful
Sentiments
Meandering silently
Yearning for release.
Hasten
Earnestly
As my love flows by
Rushing through the flood-gates
To your heart.
'''
# https://www.poetrysoup.com/poem/cross_my_heart_609765


def solve(input_data):
    '''Trả về tiêu đề bài thơ ghép từ các chữ cái đầu tiên của mỗi dòng.
    Chỉ viết hoa chữ cái đầu tiên.
    '''
    result = ''
    result_list = []
    word_list = input_data.splitlines()
    result_list = []
    for word in word_list:
        for w in word:
            if w.isupper():
                result_list.append(w)
    result = ''.join(result_list).capitalize()
    return  result


def main():
    '''
    Cross my heart là một bài thơ thuộc thể loại "acrostic".
    Khi ghép các chữ cái HOẶC các từ đầu tiên lại với nhau thu được một
    thông điệp
    '''
    print("Result should be Pymi: {}".format(solve('P\nY\nM\nI')))
    print(solve(data))

if __name__ == "__main__":
    main()
