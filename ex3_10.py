#!/usr/bin/env python3

"""
Đọc thêm (không liên quan tới bài này): https://pymi.vn/blog/bankers-rounding/

Làm thêm tùy chọn:
- https://projecteuler.net/problem=1
- https://projecteuler.net/problem=2
- https://projecteuler.net/problem=3
- https://projecteuler.net/problem=4
- https://projecteuler.net/problem=5
- https://projecteuler.net/problem=6
- https://projecteuler.net/problem=7
- https://projecteuler.net/problem=8
- https://projecteuler.net/problem=9
- https://projecteuler.net/problem=10
- https://projecteuler.net/problem=16
"""
data = (
    [1, 5, 2, 4, 3, 4],
    [4, 5, 0, 4]
)


def solve(list1, list2):
    '''Find common elements of two given lists. No duplicate in result,
    ordered by ascending.

    Returns a list contains those elements.
    Require: use only lists, if/else and for loops.
    '''
    res = []
    result = []
    for i in list2:
        for j in list1:
            if i == j and j not in res:
                res.append(j)
    result = res
    return result


def main():
    L1, L2 = data
    print(solve(L1, L2))


if __name__ == "__main__":
    main()
